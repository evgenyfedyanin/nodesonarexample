const request = require('supertest')
const app = require("../app");
const sum = require('../sum');
const coinFlip = require('../coinFlip');

describe('Get Endpoints', () => {
    test("Get", async () => {
        const response = await request(app).get("/");
        expect(response.body).toHaveProperty('name');
        expect(response.statusCode).toBe(200);
    });
})

describe.skip('my other suite', () => {
    test('my only true test', () => {
      expect(1 + 1).toEqual(1);
    });
  });

describe('Some tests', () => {
    it('Testing to see if Jest works', () => {
        expect(1).toBe(1)
    });

    test('adds 1 + 2 to equal 3', () => {
        expect(sum(1, 2)).toBe(4);
    });

    for (let index = 0; index < 5; index++) {
        test.skip('my only true test', () => {
            expect(1 + 1).toEqual(1);
        });
    }

    for (let index = 0; index < 18; index++) {
        test('coinFlip', () => {
            expect(coinFlip()).toBe('heads');
        });
    }

    for (let index = 0; index < 12; index++) {
        test('coinFlip', () => {
            expect(coinFlip()).toBe('tails');
        });
    }
})