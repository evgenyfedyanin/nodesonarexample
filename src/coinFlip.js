function coinFlip() {
    return (Math.random() <= 0.5) ? 'heads' : 'tails';
}
module.exports = coinFlip;